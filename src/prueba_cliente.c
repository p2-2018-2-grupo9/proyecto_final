#include "logdb.h"

int main(int argc, char **argv){			//archivo de prueba para la ejecucion del programa logdb
	conexionlogdb *con = conectar_db("127.0.0.1",45000);

	printf("%d\n",crear_db(con,"base_5"));

	printf("%d\n",abrir_db(con,"base_5"));

	printf("%d\n",put_val(con, "kroos", "8"));
	printf("%d\n",put_val(con, "Benzema", "9"));
	printf("%d\n",put_val(con, "Ramos", "4"));


	printf("%d\n",put_val(con, "Bale", "11"));
	printf("%d\n",put_val(con, "Modric", "10"));
	//printf("%d\n",eliminar(con,"Bale"));


	char *valor = get_val(con, "Ramos");
	if(valor==NULL){
		printf("Ramos no tiene numero: %s\n","null");
	}
	else{
		printf("El numero de Ramos es: %s\n",valor);
	}

	valor = get_val(con, "Bale");
	if(valor==NULL){
		printf("Bale no tiene numero: %s\n","null");
	}
	else{
		printf("El numero de Bale es: %s\n",valor);
	}

	printf("%d\n",eliminar(con,"Bale"));

	valor = get_val(con, "Bale");
	if(valor==NULL){
		printf("Bale no tiene numero: %s\n","null");
	}
	else{
		printf("El numero de Bale es: %s\n",valor);
	}

	cerrar_db(con);

	return 0;
}
