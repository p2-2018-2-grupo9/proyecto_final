
all: lib/liblogdb.so bin/logdb bin/prueba

lib/liblogdb.so: obj/logdb.o obj/cliente.o
	gcc -Wall -shared obj/logdb.o obj/cliente.o -o lib/liblogdb.so

obj/logdb.o: src/logdb.c
	gcc -fPIC -I include/ -c $^ -o $@

obj/cliente.o: src/cliente.c
	gcc -Wall -fPIC -I include/ -c $^ -o $@


#creacion del ejecutable logdb(bin/logdb), para lo cual se hace uso del
# archivo binario "logdb", de la libreria "liblogdb.so" y de la bandera "-Llib"
bin/logdb:
	gcc -Wall obj/logdb.o lib/liblogdb.so -Llib -o bin/logdb


#creacion del ejecutable prueba(bin/prueba), para lo cual se hace uso del
#archivo binario "obj/prueba_cliente.o", de la libreria "liblogdb.so"
#y la bandera "-Llib"
bin/prueba: obj/prueba_cliente.o
	gcc -Wall obj/prueba_cliente.o lib/liblogdb.so -Llib -o bin/prueba

obj/prueba_cliente.o: src/prueba_cliente.c
	gcc -Wall -fPIC -I include/ -c $^ -o $@

.PHONY: clean
clean:
	rm bin/* obj/* lib/*
