#include "uthash.h"
#include "logdb.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <signal.h>
#include <netdb.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

typedef struct test{			//objeto_(base de dato) para la hastable
	char *clave;			//puntero al nombre de la base de datos
	char *valor;			//puntero a la ruta del indice de la base de datos
	UT_hash_handle hh;		//necesario para poder usar la estrutura en la hashtable.
}objeto_db_ht;

typedef struct obj_hashtable{			//objeto_(indice) para la hastable
	char *clave;			//puntero a la clave de la base log
	long valor;				//posicion de la clave de la base log(cambiar a long)
	UT_hash_handle hh;
}objeto_indice_ht;

typedef struct base{			//objeto_(base de dato)
	FILE *base_fd;					//archivo db_log
	char *ruta_nombre_db;				//puntero a la ruta del nombre de la base de datos
	char *ruta_indice_bd;				//puntero a la ruta del indice de la base de datos
	objeto_indice_ht *indice_ht;			//puntero a un objeto_(indice-hastable)
}objeto_db;

char *archivo;			//puntero a la direccion del directorio donde se guardaran los log y los indices
objeto_db_ht *uth_bases = NULL;

void ventanilla(int sockfd_servidor2);
void op_crear(char *buf, int sockfd_servidor2);
objeto_db *op_abrir(char *buf, int sockfd_servidor2);
void op_put(objeto_db *base_abierta, char *buf, int sockfd_servidor2);
void op_get(objeto_db *base_abierta, char *buf, int sockfd_servidor2);
void op_eliminar(objeto_db *base_abierta, char *buf, int sockfd_servidor2);
void op_cerrar(objeto_db *base_abierta, int sockfd_servidor2);
void volcado_bases_uth(char *configuraciones_txt, objeto_db_ht **uth_bases2);
int cursor_final(FILE *fd);
char *just_malloc(char *buf);
void reemplazar(char *buf, char viejo, char nuevo);
void inicializar_socket_servidor(struct sockaddr_in *pdireccion_servidor, char *argv1, char *argv2);
int inicializar_servidor(int type, const struct sockaddr *addr, socklen_t alen, int qlen);

int main(int argc, char **argv){
  if(argc != 4){
    printf("Uso: ./logdb <directorio> <direccion_ipv4> <numero de puerto_conocido>\n");
    return 1; //prueba con exit(-1)
  }
		//asignando el nombre del archivo a un apuntador
    archivo = (char *)malloc(sizeof(char)*strlen(argv[1]) + 10);  //prueba poniendo un valor de malloc bien grande
    strcpy(archivo,argv[1]);

		//int puerto_conocido = atoi(argv[3]);			//asignando el puerto_conocido
		//char *ip_servidor = argv[2];							//asignando la ip del servidor

		struct sockaddr_in direccion_servidor;     //Direccion del servidor
		inicializar_socket_servidor(&direccion_servidor,argv[2],argv[3]);		//inicializa los parametros del socket_servidor

		int sockfd_servidor;		//socket del servidor
		if( (sockfd_servidor = inicializar_servidor(SOCK_STREAM, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor), 1000)) < 0){	//Hasta 1000 solicitudes en cola
			printf("Error: al inicializar el servidor!\n");
			return 1;		//prueba con exit(-1)
		}

		volcado_bases_uth("configuraciones.txt", &uth_bases);
		while(1){
			int sockfd_servidor2 = accept(sockfd_servidor, NULL, NULL);
			if(sockfd_servidor2 < 0){			//validando el sockfd_servidor2
				printf("Error: al aceptar la solicitud!\n");
				return -1;
			}
			ventanilla(sockfd_servidor2);			//Llamndo a la funcion ventanilla
		}

		return 0;
}

void inicializar_socket_servidor(struct sockaddr_in *pdireccion_servidor, char *argv1, char *argv2){
  int puerto = atoi(argv2);
  memset(pdireccion_servidor, 0, sizeof(struct sockaddr_in));	//Ponemos en 0 la estructura direccion_servidor
  (*pdireccion_servidor).sin_family = AF_INET;		//IPv4
	(*pdireccion_servidor).sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	(*pdireccion_servidor).sin_addr.s_addr = inet_addr(argv1) ;	//Nos vinculamos a la interface localhost o podemos usar INADDR_ANY para ligarnos A TODAS las interfaces
}

int inicializar_servidor(int type, const struct sockaddr *addr, socklen_t alen, int qlen){
	int fd;
	int err = 0;

	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;

	if(bind(fd, addr, alen) < 0)
		goto errout;

	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){
			if(listen(fd, qlen) < 0)
				goto errout;
	}

	return fd;

  errout:
	err = errno;
	close(fd);
	errno = err;

	return (-1);
}

void ventanilla(int sockfd_servidor2){			//esta es la funcion que recibira las solicitudes del programa cliente (ventanilla), y dependiendo de la peticion determinara que se realizara
	int cuenta, opcion = 0;
	char buf[LINEA_LLENA] = {0};			//es necesario un buf de char, para leer el archivo configuraciones.txt linea por linea
	char *buf2;
	objeto_db *base_abierta = NULL;			//para obtener un puntero a un objeto

	while((cuenta = read(sockfd_servidor2,buf,sizeof(buf))) > 0){			//esto nos permite eliminar el caracter de salto de linea presente en la linea nombre_base:direccion_base
		buf2 = buf;
		while(*buf2){
			if(*buf2=='\n'){
				*buf2 = '\0';
			}
			buf2++;
		}

		buf2 = (buf + 2);		//Apuntando el nombre de la base(aritmetica de punteros)
		buf[1] = '\0';		//Haciendo nulo('\0') el separador del nombre de la base y la opcion elegida

		opcion = atoi(buf);			//convirtiendo la opcion en un numero
		switch(opcion){			//con el switch se podra determinar que tipo de accion requiere el cliente que sea realizada
			case 0:
					op_crear(buf2, sockfd_servidor2);			//esta opcion permitira crear una base
					break;

			case 1:
					base_abierta = op_abrir(buf2, sockfd_servidor2);			//esta opcion permitira abrir una base
					break;

			case 2:
					op_put(base_abierta, buf2, sockfd_servidor2);			//esta opcion permitira agregar elementos a una base abierta
					break;

			case 3:
					op_get(base_abierta, buf2, sockfd_servidor2);			//esta opcion permitira obtener elementos de una base abierta
					break;

			case 4:
					op_eliminar(base_abierta, buf2, sockfd_servidor2);			//esta opcion permitira eliminar elementos de una base abierta
					break;

			case 5:
					op_cerrar(base_abierta, sockfd_servidor2);			//esta opcion permitira cerrar una base
					break;
		}			//fin del switch

	}			//fin del while

}

void op_crear(char *buf, int sockfd_servidor2){	//permitira crear la base de datos solicitada
	char ack[3];			//ack(acknowledgement) o acuse de recibo para confirmar la creacion correcta o el creacion fallida de la base al cliente
	ack[0] = '1';
	char buf2[LINEA_LLENA] = {0};			//necesario inicializar para los envio de acknowledgement
	strcpy(buf2,archivo);
	strcat(buf2,"/");
	strcat(buf2,buf);
	strcat(buf2,"_logdb");

	objeto_db_ht *buscar = NULL;
	HASH_FIND_STR(uth_bases,buf,buscar);
	if(buscar==NULL){
		int fd = open(buf2, O_WRONLY | O_CREAT, 0777);
		if(fd<0){
			printf("Error: al crear el archivo %s\nNota: La respuesta en el cliente a esta accion es:\n1 (se creo la base)\n0 (no se creo la base)\n2 (la base ya existe)",buf2);
			ack[0] = '0';			//error de creacion de archivo
		}else{
			char buf_configuraciones[LINEA_LLENA] = {0};
			FILE *configuraciones_fd = fopen("configuraciones.txt","a");
			if(configuraciones_fd == NULL){
				printf("Error: al abrir el archivo configuraciones.txt\n!");
			}
			strcpy(buf_configuraciones,buf);			//concatenando el nombre y la ruta de base_log
			strcat(buf_configuraciones,":");
			strcat(buf_configuraciones,buf2);
			strcat(buf_configuraciones,"\n");
			fputs(buf_configuraciones,configuraciones_fd);			//permite llenar linea a linea un archivo
			fclose(configuraciones_fd);			//permite cerrar el fichero(FILE)

			objeto_db_ht *obj = malloc(sizeof(objeto_db_ht));			//se agrega la nueva base a la hastable de bases de datos
			obj->clave = just_malloc(buf);			//Esto es necesario pra que cada base(clave) tenga su propia referencia en la hastable el just_malloc crea un espacio en memoria para el buf
			obj->valor = just_malloc(buf2);			//Esto es necesario para que se cree un malloc para la ruta de base_log y reenviarla a la hastable
			char *clave = obj->clave;
			HASH_ADD_STR(uth_bases,clave,obj);			//agrega el nuevo elemento a la hashtable

			close(fd);			//permite cerrar el descriptor de archivo
		}
		write(sockfd_servidor2,ack,sizeof(ack));			//envia la confirmacion del ack
	}else{
		ack[0] = '2';
		write(sockfd_servidor2,ack,sizeof(ack));			//la base ya esta creada
	}

}

objeto_db *op_abrir(char *buf, int sockfd_servidor2){			//permite abrir una base de datos
	objeto_db_ht  *objeto_buscado = NULL;
	objeto_db *base_abierta = NULL;
  HASH_FIND_STR(uth_bases, buf, objeto_buscado);
	char ack[3];
  if(objeto_buscado!=NULL){
		char buf2[LINEA_LLENA] = {0};			//necesario inicializar para los envio de acknowledgement
		strcpy(buf2,archivo);
		strcat(buf2,"/");
		strcat(buf2,buf);
		strcat(buf2,"_logdb");			//concatenamos en buf2, la direccion de la base logdb
		char buf3[LINEA_LLENA] = {0};			//necesario inicializar para los envio de acknowledgement
		strcpy(buf3,archivo);
		strcat(buf3,"/");
		strcat(buf3,buf);
		strcat(buf3,"_indicedb");			//concatenamos en buf3, la direccion de indicedb

		FILE *log_base_fd = fopen(buf2,"a+");			//abrimos el archivo de la base logdb
		if(log_base_fd!=NULL){
			FILE *indice_base_fd = fopen(buf3,"a+");			//abrimos el archivo indicedb
			if(indice_base_fd!=NULL){
				objeto_indice_ht *indice_ht = NULL;
				char buf4[LINEA_LLENA];

				while(fgets(buf4,LINEA_LLENA,indice_base_fd)!=NULL){			//volcado del archivo db_indice a una hastable
					objeto_indice_ht *data_ht = malloc(sizeof(objeto_indice_ht));
					reemplazar(buf4,'\n','\0');
					char *separador = strchr(buf4,':');
					*separador ='\0';			//reemplazar(buf4,*separador,'\0');
					separador++;
					data_ht->clave = just_malloc(buf4);			//crea un espacio en memoria a la medida de la cadena apuntada por el puntero enviado como argumento
					data_ht->valor = atol(separador);
					char *clave = data_ht->clave;
					HASH_ADD_STR(indice_ht,clave,data_ht);
				}

				base_abierta = (objeto_db *)malloc(sizeof(objeto_db));			//confirmar si esta bien
				base_abierta->base_fd = log_base_fd;
				base_abierta->ruta_nombre_db = just_malloc(buf2);			//crea un espacio en memoria a la medida de la cadena apuntada por el puntero enviado como argumento
				base_abierta->ruta_indice_bd = just_malloc(buf3);
				base_abierta->indice_ht = indice_ht;
				fclose(indice_base_fd);

				ack[0] = '1';			//confirmacion exitosa del ack
				write(sockfd_servidor2,ack,sizeof(ack));			//envia la confirmacion del ack
				return base_abierta;
			}else{
				ack[0] = '2';			//error al abrir el archivo indice
				write(sockfd_servidor2,ack,sizeof(ack));			//envia la confirmacion del ack
				return NULL;
				//enviar al socket error al abrir el archivo indicie
			}
		}else{
			ack[0] = '3';			//error al abrir el archivo log
			write(sockfd_servidor2,ack,sizeof(ack));			//envia la confirmacion del ack
			return NULL;
			//enviar al socket error al abrir el archivo log
		}
  }else{
		ack[0] = '4';			//error al abrir la base(no existe)
		write(sockfd_servidor2,ack,sizeof(ack));			//envia la confirmacion del ack
		return NULL;
		//enviar al socket error no existe dicha base en el archivo configuraciones.txt
	}

}

void op_put(objeto_db *base_abierta, char *buf, int sockfd_servidor2){			//permite poner la clave-valor en una base abierta
	char ack[3];
	if(base_abierta != NULL){
		char buf2[LINEA_LLENA] = {0};			//necesario inicializar para los envio de acknowledgement
		char *separador = strchr(buf,':');
		*separador = '\0';			//Puede ser tambien buf[posicion] = '\0';
		separador++;
		strcpy(buf2,buf);			//verificar los valores que se reciben para saber si no genera errores
		strcat(buf2,":");
		strcat(buf2,separador);
		strcat(buf2,"\n");
		if(cursor_final(base_abierta->base_fd)==0){
			objeto_indice_ht *objeto_indice_buscado = NULL;
			HASH_FIND_STR(base_abierta->indice_ht, buf, objeto_indice_buscado);
		  if(objeto_indice_buscado!=NULL){
		    objeto_indice_buscado->valor = ftell(base_abierta->base_fd);
		  }else{
				objeto_indice_buscado = malloc(sizeof(objeto_indice_ht));			//separa memoria para el objeto de tipo de la hashtable
				objeto_indice_buscado->clave = just_malloc(buf);			//crea un espacio en memoria a la medida de la cadena apuntada por el puntero enviado como argumento
				objeto_indice_buscado->valor = ftell(base_abierta->base_fd);			//ftell devuelve la posicion actual del cursor del descriptor de archivo(FILE) enviado como parametro
				char *clave = objeto_indice_buscado->clave;
				HASH_ADD_STR(base_abierta->indice_ht, clave, objeto_indice_buscado);			//agrega el nuevo elemento a la hashtable
			}
			fputs(buf2,base_abierta->base_fd);			//agregando el nombre y la clave al archivo log de la base abierta
			ack[0] = '1';			//confirmacion exitosa del ack
			write(sockfd_servidor2,ack,sizeof(ack));			//envia la confirmacion del ack
		}else{
			ack[0] = '2';			//error no se pudo mover el cursor al final del archivo log
			write(sockfd_servidor2,ack,sizeof(ack));
		}
	}else{
			ack[0] = '0';			//error la base no esta abierta
			write(sockfd_servidor2,ack,sizeof(ack));
	}

}

void op_get(objeto_db *base_abierta, char *buf, int sockfd_servidor2){			//funcion para obtener el valor de una clave dada de una base abierta
	char ack[3];
	if(base_abierta != NULL){
		char buf2[LINEA_LLENA];
		char buf3[LINEA_LLENA] = {0};			//necesario inicializar para los envio de acknowledgement

		reemplazar(buf,'\n','\0');
		objeto_indice_ht *objeto_indice_buscado = NULL;
		HASH_FIND_STR(base_abierta->indice_ht, buf, objeto_indice_buscado);
		if(objeto_indice_buscado!=NULL){
			fseek(base_abierta->base_fd, objeto_indice_buscado->valor, SEEK_SET);			//moviendo el cursor a la posicion del clave en el archivo log
			fgets(buf2,LINEA_LLENA,base_abierta->base_fd);
			reemplazar(buf2,'\n','\0');
			char *separador = strchr(buf2,':');
			separador++;
			//char *valor_indice = just_malloc(separador);
			strcpy(buf3,separador);
			write(sockfd_servidor2,buf3,strlen(buf3));			//envia el buf3 con el valor de la clave
		}else{
			strcat(buf3,"@T!\n");
			write(sockfd_servidor2,buf3,strlen(buf3));			//envia el buf3 con el valor de la clave
		}
	}else{
		ack[0] = 'x';			//error la base no esta abierta
		write(sockfd_servidor2,ack,sizeof(ack));		//enviando el ack de confirmacion de error
					//error base no esta abierta
	}
}

void op_eliminar(objeto_db *base_abierta, char *buf, int sockfd_servidor2){			//permite eliminar una clave-valor
	char buf2[LINEA_LLENA]={0};
	strcat(buf2,buf);
	strcat(buf2,":");
	strcat(buf2,"@T!");			//se concatena a la clave(buf2) el valor de eliminacion: "@T!", el cual representara en la base logdb que una clave ha sido elminada
	op_put(base_abierta, buf2, sockfd_servidor2);			//se llama a la funcion back-end: "op_put", la cual pondra la clave con el valor de eliminacion
}

void op_cerrar(objeto_db *base_abierta, int sockfd_servidor2){			//permite cerrar una conexion!
	char ack[3];
	char buf[LINEA_LLENA];
	if(base_abierta != NULL){
			FILE *indice_fd = fopen(base_abierta->ruta_indice_bd, "w");			//abrimos el archivo "indicedb", nota: truncamos el archivo y reescribimos desde el inicio del mismo
			if(indice_fd!=NULL){
					objeto_indice_ht *elemento, *tmp, *remover;
					HASH_ITER(hh, base_abierta->indice_ht, elemento, tmp){			//hacemos uso de la funcion "HASH_ITER", la cual nos permitira iterar sobre la hastable-indice a la vez que vamos escribiendolos en el archivo indicedb y los vamos eliminando de la hastable
							sprintf(buf,"%s:%ld\n", elemento->clave, elemento->valor);			//introducimos la clave y el valor sobre el buffer "buf" en el formato adecuado
							fputs(buf, indice_fd);			//escribimos el archivo "indicedb" con el contenido de buf
							HASH_FIND_STR(base_abierta->indice_ht, elemento->clave, remover);			//buscamos el elemento iterado en la hastable al cual hara referencian el puntero "remover"
							HASH_DEL(base_abierta->indice_ht, remover);			//removemos el elemento iterado apuntado por remover
							free(remover);			//liberamos la memoria asignada a remover
					}
					fclose(indice_fd);			//cerrando el archivo indice
					fclose(base_abierta->base_fd);			//cerramos el archivo log de la base abierta
					free(base_abierta->ruta_nombre_db);			//liberando el puntero a la ruta de log
					free(base_abierta->ruta_indice_bd);			//liberando el puntero a la ruta del indice

					ack[0] = '1';			//confirmacion exitosa del ack
					write(sockfd_servidor2,ack,sizeof(ack));			//envio de confirmacion de cierre exitoso
			}else{
				ack[0] = '2';			//confirmacion exitosa del ack
				write(sockfd_servidor2,ack,sizeof(ack));
				//no se pudo abrir el archivo indice
			}
	}else{
		ack[0] = '0';			//confirmacion exitosa del ack
		write(sockfd_servidor2,ack,sizeof(ack));
					//la base no esta abierta
	}
}

char *just_malloc(char *buf){			//MOVER: permite crear un malloc a la medida(tamanio) de la palabra a la que apunta buf
	return strdup(buf);
}

void volcado_bases_uth(char *configuraciones_txt, objeto_db_ht **uth2){
	char *separador;
	char buf[LINEA_LLENA] = {0};

	FILE *fd = fopen(configuraciones_txt,"r");			//solo para lectura
	if(fd==NULL){
		printf("Error: al abrir el archivo configuracion.txt (Recuerde: para ejecutar el ./logdb\ndebe estar afuera de la carpeta bin es decir: ./bin/logdb)!\n");
		exit(-1);			//mira si no genera error
	}

	while(fgets(buf, LINEA_LLENA, fd) != NULL){		//esto se puede poner en una mini_funcion
		objeto_db_ht *obj = malloc(sizeof(objeto_db_ht));
		char *buf_r = buf;
		while(*buf_r){
			if(*buf_r=='\n'){
				*buf_r= '\0';
			}
			buf_r++;
		}
		separador = strchr(buf,':');			//devuelve un apuntador a la division de la cadena representada por el caracter ":"
		int cursor = (separador-buf);
		cursor++;			//se desplaza una posicion por delante de la posicion del caracter divisor de cadena ":"
		buf_r = buf;
		while(*buf_r){
			if(*buf_r==':'){
				*buf_r= '\0';
			}
			buf_r++;
		}
		char *buf2 = just_malloc(buf + cursor);			//crea un espacio en memoria a la medida de la cadena apuntada por el puntero enviado como argumento
		obj->clave = just_malloc(buf);			//Esto es necesario pra que cada base(clave) tenga su propia referencia en la hastable el just_malloc crea un espacio en memoria para el buf
		obj->valor = buf2;
		char *clave = obj->clave;
		HASH_ADD_STR(uth_bases, clave, obj);
	}

}

int cursor_final(FILE *fd){			//ubica el cursor al final del archivo enviado como parametro
	return fseek(fd,0,SEEK_END);
}

void reemplazar(char *buf, char viejo, char nuevo){			//reemplaza el caracter "viejo" por el caracter "nuevo" dentro de un buffer enviado como parametro en "buf"
	while(*buf){
		if(*buf==viejo){
			*buf = nuevo;
		}
		buf++;
	}
}
