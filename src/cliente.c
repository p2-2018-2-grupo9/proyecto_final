#include "logdb.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <signal.h>
#include <netdb.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#define LINEA_LLENA 8192

#define MAXSLEEP 64

int n = 0;      //servira como valor-semilla que se pasara como argumento a la funcion srand, para randomizar de tal manera que
                //los valores aleatorios se generaran en funcion del numero que reciba n(0 en este caso)

char *just_malloc(char *buf);
void reemplazar(char *buf, char viejo, char nuevo);
void inicializar_socket_cliente(struct sockaddr_in *pdireccion_cliente, char *argv1, int argv2);
static int inicializar_cliente( int domain, int type, int protocol, const struct sockaddr *addr, socklen_t alen);
conexionlogdb *conectar_db(char *ip_v4, int puerto);
int crear_db(conexionlogdb *conexion, char *nombre_base);
int abrir_db(conexionlogdb *conexion, char *nombre_base);
int put_val(conexionlogdb *conexion, char *clave, char *valor);
char *get_val(conexionlogdb *conexion, char *clave);
int eliminar(conexionlogdb *conexion, char *clave);


void cerrar_db(conexionlogdb *conexion){  //esta funcion permite cerrar la conexion
  int tamanio;
  char buf[LINEA_LLENA] = "5 ";     //"5 ", corresponde a la accion de la funcion para que sea reconocida por el back-end
  char ack[10];                   //acknowledgement o acuses de recibo para los mensajes de exito o error
  if((conexion->nombredb)!= NULL){      //en el caso de que no se pueda abri la base
     strcat(buf,conexion->nombredb);
  }
  strcat(buf,"\n");
  tamanio = strlen(buf);

  write(conexion->sockfd,buf,tamanio);      //probar enviando sizeof(buf) como tamanio
  read(conexion->sockfd,ack,sizeof(ack));   //recibiendo el ack

  switch (ack[0]) {     //validando el resultado del ack
    case '1':
      printf("%-11s La conexion se cerro exitosamente: 0\n", "CERRAR.-");
      free(conexion->ip);     //liberando el puntero a la ip de la conexion
      free(conexion->nombredb);     //liberando el puntero al nombre de la conexion
      close(conexion->sockfd);      //cerrando el socket del cliente de la conexion
      break;

    case '2':
      printf("%-11s Error: no se pudo cerrar la conexion: -1\n", "CERRAR.-");
      exit(-1);
      break;

    case '0':
      printf("%-11s La conexion se cerro exitosamente: 0\n", "CERRAR.-");
      free(conexion->ip);     //liberando el puntero a la ip de la conexion
      close(conexion->sockfd);      //confirmar si este case es permitido
      break;
  }

}

int eliminar(conexionlogdb *conexion, char *clave){     //permite eleminar un elemento de la base logdb
  int tamanio;
  char buf[LINEA_LLENA] = "4 ";     //"4 ", corresponde a la accion de la funcion para que sea reconocidad por el back-end
  char ack[10];     //acknowledgement o acuses de recibo para los mensajes de exito o error
  strcat(buf,clave);
  strcat(buf,"\n");
  tamanio = strlen(buf);

  write(conexion->sockfd,buf,tamanio);      //probar enviando sizeof(buf) como tamanio
  read(conexion->sockfd,ack,sizeof(ack));   //recibiendo el ack

  switch (ack[0]) {     //validando el resultado del ack
    case '1':
      printf("%-11s Se elimino exitosamente %s de la base: ", "ELIMINAR.-",clave);
      break;

    case '2':
      printf("%-11s Error: no se pudo eliminar %s de la base: ", "ELIMINAR.-", clave);
      return -1;

    case '0':
      printf("%-11s Error: debe abrir la base primero: ", "ELIMINAR.-");
      return -1;
  }

  return 0;

}




char *get_val(conexionlogdb *conexion, char *clave){      //permite obtener un valor de la base logdb
  int tamanio;
  char buf[LINEA_LLENA] = "3 ";     //"3 ", corresponde a la accion de la funcion para que sea reconocidad por el back-end
  char buf2[LINEA_LLENA] = {0};
  strcat(buf,clave);
  strcat(buf,"\n");
  tamanio = strlen(buf);

  write(conexion->sockfd,buf,tamanio);      //probar enviando sizeof(buf) como tamanio
  read(conexion->sockfd,buf2,sizeof(buf2));   //recibiendo el ack
  reemplazar(buf2,'\n','\0');     //prueba sin esto y el strcmp(buf2,"@T!\n");
  if(strcmp(buf2,"@T!")!=0){
    if(buf2[0]!='x'){
      printf("%-11s ", "GET.-");
      return just_malloc(buf2);
    }else{
      printf("%-11s Error: debe abrir la base primero: ", "GET.-");
      return NULL;     //comprueba si no causa error
    }
  }else{
      printf("%-11s La clave: %s no existe en la base: ", "GET.-", clave);
      return NULL;
  }
}


int put_val(conexionlogdb *conexion, char *clave, char *valor){     //permite poner claves y valores a la base logdb
  int tamanio;
  char buf[LINEA_LLENA] = "2 ";     //"2 ", corresponde a la accion de la funcion para que sea reconocidad por el back-end
  char ack[10];
  strcat(buf,clave);
  strcat(buf,":");
  strcat(buf,valor);
  strcat(buf,"\n");
  tamanio = strlen(buf);

  write(conexion->sockfd,buf,tamanio);      //probar enviando sizeof(buf) como tamanio
  read(conexion->sockfd,ack,sizeof(ack));   //recibiendo el ack

  switch (ack[0]) {     //validando el resultado del ack
    case '1':
      printf("%-11s Se agrego exitosamente \"%s:%s\" a la base: ", "PUT.-", clave, valor);
      break;

    case '2':
      printf("%-11s Error: no se pudo agregar %s:%s a la base: ", "PUT.-", clave, valor);
      return -1;

    case '0':
      printf("%-11s Error: debe abrir la base primero: ", "PUT.-");
      return -1;
  }

  return 0;
}


int abrir_db(conexionlogdb *conexion, char *nombre_base){     //permite abrir una base logdb
  int tamanio;
  char buf[LINEA_LLENA] = "1 ";     //"1 ", corresponde a la accion de la funcion para que sea reconocidad por el back-end
  char ack[10];
  strcat(buf,nombre_base);
  strcat(buf,"\n");
  tamanio = strlen(buf);

  write(conexion->sockfd,buf,tamanio);      //probar enviando sizeof(buf) como tamanio
  read(conexion->sockfd,ack,sizeof(ack));   //recibiendo el ack

  switch (ack[0]) {     //validando el resultado del ack
    case '1':
      printf("%-11s La base se abrio exitosamente: ", "ABRIR.-");
      conexion->nombredb = just_malloc(nombre_base);      //asigna el nombre de la base abierta al atributo nombredb de la conexion!
      break;

    case '2':
      printf("%-11s Error al abrir el archivo: %s_indice: ", "ABRIR.-", nombre_base);
      return -1;

    case '3':
      printf("%-11s Error al abrir el archivo: %s_log: ", "ABRIR.-" , nombre_base);
      return -1;

    case '4':
      printf("%-11s Error: la base %s no existe debe crearla primero: ", "ABRIR.-", nombre_base);
      return -1;
  }

  return 0;

}

int crear_db(conexionlogdb *conexion, char *nombre_base){     //permite crear una base logdb
  int tamanio;
  char buf[LINEA_LLENA] = "0 ";     //"0 ", corresponde a la accion de la funcion para que sea reconocidad por el back-end
  char ack[10];
  strcat(buf,nombre_base);
  strcat(buf,"\n");
  tamanio = strlen(buf);

  write(conexion->sockfd,buf,tamanio);      //probar enviando sizeof(buf) como tamanio
  read(conexion->sockfd,ack,sizeof(ack));   //recibiendo el ack

  switch (ack[0]) {     //validando el resultado del ack
    case '1':
      printf("%-11s La base se creo exitosamente: ","CREAR.-");
      break;

    case '2':
      printf("%-11s La base ya existe: ","CREAR.-");
      return -1;

    case '0':
      printf("%-11s Error: La base no se creo: ","CREAR.-");
      return -1;
  }

  return 0;
}


conexionlogdb *conectar_db(char *ip_v4, int puerto){      //permite entablar la conexion desde el front-end
  if( ip_v4!=NULL && puerto >=0 ){
    int sockfd;
    struct sockaddr_in direccion_cliente;
    inicializar_socket_cliente(&direccion_cliente,ip_v4,puerto);      //llama a la funcion inicializar socket_cliente para obtener los valores del socket_cliente inicializados
    conexionlogdb *conexion;

    if(( sockfd = inicializar_cliente( direccion_cliente.sin_family, SOCK_STREAM, 0, (struct sockaddr *)&direccion_cliente, sizeof(direccion_cliente))) < 0){
      printf("Error: falló de conexión\n");
      return NULL;
    }

    conexion = (conexionlogdb *) malloc(sizeof(conexionlogdb));
    conexion->ip = just_malloc(ip_v4);
    conexion->puerto = puerto;
    conexion->sockfd = sockfd;
    if(!n){
      srand(time(NULL));      //randomizamos
    }
    conexion->id_sesion = rand();     //genera los valores aleatorios
    conexion->nombredb = NULL;
    printf("_________________________________________\n");
    printf("Conexion exitosa:\nip: %s\npuerto: %d\n", ip_v4, puerto);
    printf("_________________________________________\n");
    printf("ACCION.-  | MENSAJE: \"0(EXITO)/-1(ERROR)\"\n");
    printf("-----------------------------------------\n");
    printf("%-11s La conexion se realizo exitosamente: 0\n","CONECTAR.-");
    return conexion;
  }else{
    printf("%-11s Error: la ip esta vacia o el puerto es incorrecto: -1\n","CONECTAR.-");
    return NULL;
  }

}

void inicializar_socket_cliente(struct sockaddr_in *pdireccion_cliente, char *argv1, int argv2){      //esta funcion permite inicializar el socket del cliente
  memset(pdireccion_cliente, 0, sizeof(struct sockaddr_in));	//Ponemos en 0 la estructura direccion_servidor
  (*pdireccion_cliente).sin_family = AF_INET;		//IPv4
	(*pdireccion_cliente).sin_port = htons(argv2);		//Convertimos el numero de puerto al endianness de la red
	(*pdireccion_cliente).sin_addr.s_addr = inet_addr(argv1) ;	//Nos tratamos de conectar a esta direccion
}

static int inicializar_cliente( int domain, int type, int protocol, const struct sockaddr *addr, socklen_t alen){     //esta funcion permite inicializar el cliente
	int numsec, fd;

	for (numsec = 1; numsec <= MAXSLEEP; numsec <<= 1) {
		if (( fd = socket( domain, type, protocol)) < 0){
      return -1;
    }

		if (connect( fd, addr, alen) == 0) {
			return(fd);
		}

		close(fd);

		if (numsec <= MAXSLEEP/2){
      sleep( numsec);
    }
	}

	return -1;
}





